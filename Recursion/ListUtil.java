package Recursion;

import java.util.Arrays;
import java.util.List;

import coinpurse.Valuable;

/**
 * ListUtil For exercising Recursion
 * @author Voraton Lertrattanapaisal
 *
 */
public class ListUtil {
	/**
	 * For print list without loop
	 * @param list that want to print
	 */
	 private static void printList(List<?> list) {
	 	if(list.size()!=0){
	 		if (list.size()==1){
		 		System.out.print(list.get(0));
	 		}
	 		else {
	 			System.out.print(list.get(0)+", ");
	 		}
	 		printList(list.subList(1, list.size()));
	 	}
	 }
	 /**
	 * Find the largest element in a List of Strings,
	 * using the String compareTo method.
	 * @return the lexically largest element in the List
	 */
	 private static String max( List<String> list) {
		 if (list==null){
			 return null;
		 }
		 if (list.size()==1){
			 return list.get(0);
		 }
		 else {
			 if (list.get(0).compareTo(list.get(list.size()-1))>0){
				 return max(list.subList(0, list.size()-1));
			 }
			 else {
				 return max(list.subList(1, list.size()));
			 }
		 }
	 }

	 /** Test the max method. */
	 public static void main(String [] args) {
	 List<String> list;
	 // if any command line args, then use them as the list!
	 if (args.length > 0) list = Arrays.asList( args );
	 else list = Arrays.asList("bird", "zebra", "cat", "pig");
	 System.out.print("List contains: ");
	 printList( list );
	 String max = max(list);
	 System.out.println("Lexically greatest element is "+max);
	 
	 }
	}
