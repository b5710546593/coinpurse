package coinpurse;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * To display balance of purse in text.
 * @author Voraton Lertrattanapaisal
 *
 */
public class PurseObserver implements Observer{
	/**
	 * Initialize PurseObserver.
	 */
	public PurseObserver(){
		//no reference to purse necessary
	}
	/** update receives notification from the purse */
	public void update(Observable subject,Object info){
		if (subject instanceof Purse){
			Purse purse = (Purse)subject;
			int balance = (int)purse.getBalance();
			System.out.println("Balance is: "+balance);
		}
		if (info != null) System.out.println(info);
	}
}

