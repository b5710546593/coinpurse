package coinpurse;
/**
 * Valuable is interface that contain getValue();
 *  
 *  @author your Voraton Lertrattanapaisal
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * getValue() is use for return the value
	 * @return double 
	 */
	public double getValue( );
}