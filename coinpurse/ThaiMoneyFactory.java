package coinpurse;

import java.util.Arrays;
import java.util.List;

public class ThaiMoneyFactory extends MoneyFactory {

	private final List<Double> coinList = Arrays.asList(1.0,2.0,5.0,10.0);
	private final List<Double> bankList = Arrays.asList(20.0, 50.0, 100.0, 500.0, 1000.0);
	private final String currency = "Thai";
	@Override
	Valuable createMoney(double value) {
		if (coinList.contains(value)) return new Coin(value,currency);
		else if (bankList.contains(value)) return new BankNote(value,currency);
		else throw new IllegalArgumentException();
	}

	@Override
	Valuable createMoney(String value) {
		double dvalue = Double.parseDouble(value);
		if (coinList.contains(dvalue)) return new Coin(dvalue,currency);
		else if (bankList.contains(dvalue)) return new BankNote(dvalue,currency);
		else throw new IllegalArgumentException();
	}
	
}
