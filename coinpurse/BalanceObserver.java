package coinpurse;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * To show balance of purse in windows
 * @author Voraton Lertrattanapaisal
 *
 */
public class BalanceObserver extends JFrame implements Observer {
	/**
	 * Balance of purse.
	 */
	private int balance;
	/**
	 * To initialize BalanceObserver.
	 */
	public BalanceObserver(){
		super("Purse Balance");
		balance = 0;
		initialize();
	}
	/** Label to show balance.*/
	JLabel balanceLabel;
	/**
	 * To intialize the JComponent.
	 */
	public void initialize(){
		balanceLabel = new JLabel(balance+" Baht");
		super.getContentPane().setLayout(new GridLayout());
		super.getContentPane().add(balanceLabel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	/**
	 * Update balance of windows.
	 */
	public void update(Observable subject, Object info){
		if (subject instanceof Purse){
			Purse purse = (Purse)subject;
			balance = (int)purse.getBalance();
			balanceLabel.setText(balance+" Baht");
			super.repaint();
		}
		if (info != null) System.out.println(info);
	}
	/**
	 * For run the window.
	 */
	public void run(){
		pack();
		setVisible(true);
	}
}
