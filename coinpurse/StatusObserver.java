package coinpurse;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
/**
 * To show status bar of number of object in purse
 * @author Voraton Lertrattanapaisal
 *
 */
public class StatusObserver  extends JFrame implements Observer {
	/**
	 * Initialize StatusObserver.
	 */
	public StatusObserver(){
		super("Purse Status");
		initialize();

	}
	/**
	 * Bar that show number of object in percent.
	 */
	JProgressBar bar;
	/**
	 * Label to display number of Object.
	 */
	JLabel balanceLabel;
	/**
	 * To intialize the JComponent.
	 */
	public void initialize(){
		balanceLabel = new JLabel();
		balanceLabel.setText("EMPTY");
		bar = new JProgressBar();
		super.getContentPane().setLayout(new GridLayout(2,1));
		super.getContentPane().add(balanceLabel);
		super.getContentPane().add(bar);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	/**
	 * Update status bar.
	 */
	public void update(Observable subject, Object info){
		if (subject instanceof Purse){
			Purse purse = (Purse)subject;
			if (purse.isFull()){
				balanceLabel.setText("FULL");
			}
			else {
				if (purse.count()!=0){
					balanceLabel.setText(purse.count()+"");
				}
				else {
					balanceLabel.setText("EMPTY");
				}
			}
			bar.setMaximum(purse.getCapacity());
			bar.setMinimum(0);
			bar.setValue(purse.count());
			super.repaint();
		}
		if (info != null) System.out.println(info);
	}
	/**
	 * For run the window.
	 */
	public void run(){
		pack();
		setVisible(true);
	}
}
