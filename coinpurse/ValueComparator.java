package coinpurse;

import java.util.Comparator;

public class ValueComparator implements Comparator<Valuable> {
	/**
	 * @param a ,b are valuables that will be compared
	 * to compare the value of valuables
	 * @return different of value of two valuables
	 */
	public int compare(Valuable a, Valuable b) {
		return (int)b.getValue()-(int)a.getValue();
	}
}
