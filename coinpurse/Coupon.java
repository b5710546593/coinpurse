package coinpurse;

import java.util.HashMap;
import java.util.Map;
/**
 * A Coupon with a monetary value.
 * You can't change the value of a Coupon.
 * @author your Voraton Lertrattanapaisal
 */
public class Coupon extends AbstractValuable {
    /** Color of the coupon */
	private String color;
	/** Map of the color of coupon and value of coupon */
	private Map <String,Double> map = new HashMap<String,Double>();
	/** 
     * Constructor for a new coupon. 
     * @param color is the value for the coupon
     */
	public Coupon(String color){
		this.color=(color.charAt(0)+"").toUpperCase()+(color.substring(1,color.length())).toLowerCase();
		map.put("Red", 100.0);
		map.put("Blue", 50.0);
		map.put("Green", 20.0);
	}
	/** To get the value of coupon
	* @return the value of coupon
	*/
	public double getValue(){
		return map.get(color);
	}
    /** 
     * To return the String 
	 * @return the human language of coupon
     */
    public String toString(){
    	
    	return String.format("%s coupon", color);
    }
}
