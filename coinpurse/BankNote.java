package coinpurse;
/**
 * A BankNote with a monetary value.
 * You can't change the value of a BankNote.
 * @author your Voraton Lertrattanapaisal
 */
public class BankNote extends AbstractValuable {
	/** Value of the BankNote */
    private double value;
    private String currency;
    /** serialnumber of the BankNote */
	private String serialNumber ;
    /** next serialnumber of the BankNote */
	private static int nextSerialNumber = 1000000;
	/** 
     * Constructor for a new BankNote. 
     * @param value is the value for the BankNote
     */
	public BankNote(double value,String currency){
		this.value=value;
		this.currency = currency;
		serialNumber=getNextSerialNumber()+"";
		nextSerialNumber ++ ;
	}
	/** 
     * To get the value of Valuable
     * @return the value of Valuable
     */
	public double getValue(){
		return value;
	}
	 /**
	  * for get next serial Number
	  * @return next serial Number
	  */
	 public static int getNextSerialNumber(){
		 return nextSerialNumber;
	 }


	 /** 
	   * To return the String 
	   * @return the human language of banknote
	  */
	 public String toString(){
	    return String.format("%.0f-%s Banknote [%s].", this.getValue(),currency,serialNumber);
	}
}
