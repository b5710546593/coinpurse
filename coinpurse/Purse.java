package coinpurse;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A money purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the money purse decides which
 *  money to remove.
 *  
 *  @author your Voraton Lertrattanapaisal
 */
public class Purse extends Observable{
    /** Collection of money in the purse. */
	List<Valuable> money = new ArrayList<Valuable>();
    /** Capacity is maximum NUMBER of money the purse can hold.
     *  Capacity is set when the purnse is created.
     */
    private int capacity;
    /** WithdrawStrategy for using method withdraw for withdraw */
    private WithdrawStrategy strategy;
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of money you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity=capacity;
    	setWithdrawStrategy(new GreedyWithdraw());
    }

    /**
     * Count and return the number of money in the purse.
     * This is the number of money, not their value.
     * @return the number of money in the purse
     */
    public int count() { return money.size(); }
    	
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double balance = 0;
    	for (int i = 0 ;i<money.size();i++){
    		balance += money.get(i).getValue();
    	}
    	return balance; 
    }

    
    /**
     * Return the capacity of the money purse.
     * @return the capacity
     */
    public int getCapacity() { return this.capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {

    	if (this.count()<this.getCapacity()){
        return false;
    	}
    	else { return true;}
    }

    /** 
     * Insert money into the purse.
     * The money is only inserted if the purse has space for it
     * and the money has positive value.  No worthless money!
     * @param value is money object to insert into purse
     * @return true if money inserted, false if can't insert
     */
    public boolean insert( Valuable value ) {
        // if the purse is already full then can't insert anything.
        if (this.isFull()){
        	return false;
        }
        else {
        	if (value.getValue()>0){
        		if (money.size()==0){
        			
            			money.add(value);
            		
        		}
        		else{
        		for (int i=0;i<money.size();i++){
        		
        			if (money.get(i).getValue()<=value.getValue()){
        				money.add(i,value);
        				break;
        			}
        			else {
        				money.add(value);
        				break;
        			}
        		}
        	}
        	super.setChanged();
        	super.notifyObservers(this);
        	return true;}
        	else {return false;
        	}
        }
    }
   
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw( double amount )
    {	
    	try{
	    	Valuable[] get =  strategy.withdraw(amount,money);
	    	if (get!=null){
				for (int i=0;i<money.size();i++){
					for (int k=0;k<get.length;k++){
						if (get[k].equals(money.get(i))){
							money.remove(i);
						}
					}
				}
	    	}
	    	super.setChanged();
	    	super.notifyObservers(this);
	    	return get;
    	}
    	catch(IllegalArgumentException x){
    		return null;
    	}
    }
    /**
     * Set the type of strategy.
     * @param strategy is strategy that I want to set.
     */
    public void setWithdrawStrategy(WithdrawStrategy strategy){
    	this.strategy=strategy;
    }
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
    	int num1 = 0;
    	int num2 = 0;
    	int num5 = 0;
    	int num10 =0;
    	for (int i = 0; i<count();i++){
    		if (money.get(i).getValue()==1){
    			num1++;
    		}
    		if (money.get(i).getValue()==2){
    			num2++;
    		}
    		if (money.get(i).getValue()==5){
    			num5++;
    		}
    		if (money.get(i).getValue()==10){
    			num10++;
    		}
    	}
    	String forreturn = "";
    	if (num1>0){
    		forreturn += String.format("%d coins with value 1-Baht ",num1);
    	}
    	if (num2>0){
    		forreturn += String.format("%d coins with value 2-Baht ",num2);
    	}
    	if (num5>0){
    		forreturn += String.format("%d coins with value 5-Baht ",num5);
    	}
    	if (num10>0){
    		forreturn += String.format("%d coins with value 10-Baht ",num10);
    	}
    	return forreturn;
    }

}
