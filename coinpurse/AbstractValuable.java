package coinpurse;
/**
 * AbstractValuable is abstract class that contain compareTo(Valuable value) and equals(Object obj);
 *  
 *  @author your Voraton Lertrattanapaisal
 */
public abstract class AbstractValuable implements Valuable  {

    /** 
	   * To compare the value of Valuable
	   * @param obj is Object to compare the value.
	   * @return the different of two valuable
	 */
	public int compareTo(Valuable value){
		return (this.getValue()+"").compareTo(value.getValue()+"");
	}
	 /** 
	   * To check is value of Valuable  equal?
	   * @param obj is Object to check if it is equal.
	   * @return is it equal
	 */
	public boolean equals(Object obj){
		if (obj==null){
	    	return false;
	    }
	    if (obj.getClass()!=this.getClass()){
	    	return false;
	    }
	    AbstractValuable other = (AbstractValuable)obj;
	    if (other.getValue()==this.getValue()){
	    	return true;
	    }
	    else { 
	    	return false;
	    }
	};
}
