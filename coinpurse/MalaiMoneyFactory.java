package coinpurse;

import java.util.Arrays;
import java.util.List;

public class MalaiMoneyFactory extends MoneyFactory {

	private final List<Double> coinList = Arrays.asList(0.05,0.10,0.20, 0.50);
	private final List<Double> bankList = Arrays.asList(1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0);
	private final String currency1 = "Sen";
	private final String currency2 = "Ringgit";

	@Override
	Valuable createMoney(double value) {
		if (coinList.contains(value)) return new Coin(value,currency1);
		else if (bankList.contains(value)) return new BankNote(value,currency2);
		else throw new IllegalArgumentException();
	}

	@Override
	Valuable createMoney(String value) {
		double dvalue = Double.parseDouble(value);
		if (coinList.contains(value)) return new Coin(dvalue,currency1);
		else if (bankList.contains(value)) return new BankNote(dvalue,currency2);
		else throw new IllegalArgumentException();
	}

}
