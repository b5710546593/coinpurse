package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author your Voraton Lertrattanapaisal
 */
public class Coin extends AbstractValuable  {

	/** Value of the Coin */
    private double value;
    private String currency;
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value,String currency ) {
    	this.value=value;
    	this.currency = currency;
    }
	/** 
     * To get the value of Coin
     * @return the value of Coin
     */
	public double getValue(){
		return value;
	}
    /** 
     * To return the String 
	   * @return the human language of coin
     */
    public String toString(){
    	if (currency == "Sen") return String.format("%.0f-%s coin.", this.getValue()*100,currency);
    	return String.format("%.0f-%s coin.", this.getValue(),currency);
    }
}
