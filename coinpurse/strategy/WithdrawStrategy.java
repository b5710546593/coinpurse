package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * A Strategy interface that contain withdraw() for withdraw money from Purse.
 * @author Voraton Lerttanapaisal
 *
 */
public interface WithdrawStrategy {
	/**
	 * For withdrawing the valuable from Purse.
	 * @param amount is amount of valuable that you want to withdraw.
	 * @param valuables is List of Valuable that I want to withdraw valuable from.
	 * @return Array of Valuable that is withdrawn.
	 */
	public Valuable[] withdraw(double amount,List<Valuable> valuables);
}
