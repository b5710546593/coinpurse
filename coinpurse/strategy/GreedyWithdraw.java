package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * Withdraw with Greedy algorithm
 * @author Voraton Lerttanapaisal
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {
	/** ValueComparator for compare the valuable's value*/
	private final ValueComparator comparator= new ValueComparator();
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @param valuables is list of valuables in purse that we want to withdraw.
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
	public Valuable[] withdraw(double amount,List<Valuable> valuables){
		//if ( ??? ) return ???;
    	if (amount<0){return null;}
	   /*
		* One solution is to start from the most valuable coin
		* in the purse and take any coin that maybe used for
		* withdraw.
		* Since you don't know if withdraw is going to succeed, 
		* don't actually withdraw the coins from the purse yet.
		* Instead, create a temporary list.
		* Each time you see a coin that you want to withdraw,
		* add it to the temporary list and deduct the value
		* from amount. (This is called a "Greedy Algorithm".)
		* Or, if you don't like changing the amount parameter,
		* use a local total to keep track of amount withdrawn so far.
		* 
		* If amount is reduced to zero (or tempTotal == amount), 
		* then you are done.
		* Now you can withdraw the coins from the purse.
		* NOTE: Don't use list.removeAll(templist) for this
		* becuase removeAll removes *all* coins from list that
		* are equal (using Coin.equals) to something in templist.
		* Instead, use a loop over templist
		* and remove coins one-by-one.		
		*/
		
    	
		// Did we get the full amount?
    	ArrayList<Valuable> newmoney = new ArrayList<Valuable>();
		if ( amount > 0 )
		{	// failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			double left = 0;
	    	for (int i = 0 ;i<valuables.size();i++){
	    		left += valuables.get(i).getValue();
	    	}
			if (amount>left){
				return  null;
			}
			else {
				
				Collections.sort(valuables,comparator);
				double balance = amount;
				for (int i =0 ;i<valuables.size();i++){
					if (balance-valuables.get(i).getValue()>=0){
						newmoney.add(valuables.get(i));
						balance-=valuables.get(i).getValue();
					}
				}
				double checkequal = 0;
				for (int i=0;i<newmoney.size();i++){
					checkequal+=newmoney.get(i).getValue();
				}
				if (checkequal!=amount){
					return  null;
				}
				
			}
		}

		// Success.
		// Since this method returns an array of money,
		// create an array of the correct size and copy
		// the money to withdraw into the array.
		Valuable[] arraymoney = new Valuable[newmoney.size()]; 
		arraymoney =	newmoney.toArray(arraymoney);
		return	arraymoney;
	}
}
