package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
/**
 * Withdraw with Recursive algorithm that make the withdraw can withdraw everything.
 * @author Voraton Lertrattanapaisal
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param valuables is list of valuables in purse that we want to withdraw.
     *  @param amount is the amount to withdraw
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
	public Valuable[] withdraw(double amount,List<Valuable> valuables){
		List <Valuable> after = withdrawFrom(amount,valuables,valuables.size()-1);
		if (after != null) {
			Valuable[] array = new Valuable[after.size()]; 
			after.toArray(array);
			return array;
		}
		return null;
	}
	/**
	 * Withdraw the requested amount of money.
	 * Return a List of money withdrawn from purse,
	 * or return null if cannot withdraw the amount requested.
	 * @param amount is the amount to withdraw 
	 * @param value is list of valuables in purse that we want to withdraw.
	 * @param lastindex is index of list of valuables in purse.
	 * @return List of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public List<Valuable> withdrawFrom(double amount,List<Valuable> value,int lastindex){
		if (amount<0){ return null; }
		if (lastindex<0){ return null; }
		if (amount-value.get(lastindex).getValue()==0) {
			List<Valuable> valueForReturn = new ArrayList<Valuable>();
			valueForReturn.add(value.get(lastindex));
			return valueForReturn;
		}
		List<Valuable> forReturn = withdrawFrom(amount-value.get(lastindex).getValue(),value,lastindex-1);
		if (forReturn==null){ forReturn = withdrawFrom(amount,value,lastindex-1); }
		else{ forReturn.add(value.get(lastindex)); }
		return forReturn;
	}
	
}
