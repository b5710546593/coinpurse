package coinpurse;

import java.util.Scanner;

import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {
	private static int CAPACITY = 10;
    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	Scanner n = new Scanner(System.in);
    	double a = n.nextDouble();
    	System.out.println(a);
        // 1. create a Purse
    	Purse purse  = new Purse (CAPACITY);
        // 2. create a ConsoleDialog with a reference to the Purse object
    	BalanceObserver balanceObserver = new BalanceObserver();
    	balanceObserver.run();
    	StatusObserver statusObserver = new StatusObserver();
    	statusObserver.run();
    	purse.addObserver(statusObserver);
    	purse.addObserver(balanceObserver);
    	ConsoleDialog ui = new ConsoleDialog( purse );
        // 3. run() the ConsoleDialog
    	ui.run();
    }
}
