package coinpurse;

import java.util.ResourceBundle;

public abstract class MoneyFactory {
	private static MoneyFactory fac;
	public static MoneyFactory getInstance(){
		setMoneyFactory();
		return fac;
	}
	public static void setMoneyFactory(MoneyFactory type){
		fac = type;
	}
	public static void setMoneyFactory(){
		ResourceBundle bundle = ResourceBundle.getBundle("purse");
		String factoryclass = bundle.getString("moneyfactory");
		try {
			fac =  (MoneyFactory)Class.forName(factoryclass).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			fac = new ThaiMoneyFactory();
		}
	}
	abstract Valuable createMoney(double value);
	abstract Valuable createMoney(String value);
}
